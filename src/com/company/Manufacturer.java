package com.company;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Manufacturer {
    private String ID, Name;
    private List<Vehicle> Vehicles;

    public Manufacturer() {
    }

    public Manufacturer(String id) {
        ID = id;
    }

    public Manufacturer(JSONObject jsonObject) {
        this.ID = jsonObject.get("id").toString();
        this.Name = jsonObject.get("name").toString();

        setVehicles((JSONArray)jsonObject.get("vehicles"));
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getID() {
        return ID;
    }

    private void setVehicles(JSONArray jsonArray) {
        if(jsonArray != null) {
            this.Vehicles = new ArrayList<>();
            for (JSONObject jsonObject : (List<JSONObject>) jsonArray) {
                this.Vehicles.add(new Vehicle(jsonObject));
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("Name: " + Name);

        if(Vehicles != null) {
            str.append("\nNumber of Vehicles: ").append(Vehicles.size());

            for (Vehicle v : Vehicles) {
                str.append("\n________________________" + "\nVIN: ").append(v.getVIN()).append("\nModel: ").append(v.getModel()).append("\nColor: ").append(v.getColor()).append("\nYear: ").append(v.getYear());
            }
        }

        return str.toString();
    }

    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        if (Name != null && !Name.isEmpty()) jsonObject.put("name", this.Name);

        return jsonObject;
    }
}
