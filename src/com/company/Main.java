package com.company;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;

class Main {

    private static final Scanner sc = new Scanner(System.in);
    public static Properties properties;

    public static void main(String[] args) throws IOException{
        properties = new Properties();
        properties.load(new FileInputStream("config.properties"));

        System.out.println("Welcome to the Inventory System");

        Menu.login();
        menuMain();

        sc.nextLine();
    }

    private static void menuMain() {
        Integer selection = 0;
        while(selection != 3) {
            System.out.println("\nMain Menu");
            System.out.println("1. Vehicles");
            System.out.println("2. Manufacturers");
            System.out.println("3. Logout");
            selection = sc.nextInt();

            switch (selection) {
                case 1:
                    Menu.vehicle();
                    break;
                case 2:
                    Menu.manufacturer();
                    break;
                case 3:
                    Menu.logout();
                    break;
                default:
                    System.out.println("Please enter a valid option.");
                    break;
            }
        }
    }
}