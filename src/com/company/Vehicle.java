package com.company;

import org.json.simple.JSONObject;

public class Vehicle {
    private String ID, VIN, Model, Color, Year, ManufacturerID;
    private Manufacturer Manufacturer;

    public Vehicle() {
    }

    public Vehicle(String id) {
        this.ID = id;
    }

    public Vehicle(JSONObject jsonObject) {
        this.ID = jsonObject.get("id").toString();
        this.VIN = jsonObject.get("vin").toString();
        this.Color = jsonObject.get("color").toString();
        this.ManufacturerID = jsonObject.get("manufacturer_id").toString();
        this.Model = jsonObject.get("model").toString();
        this.Year = jsonObject.get("year").toString();

        if(jsonObject.get("manufacturer") != null) {
            this.Manufacturer = new Manufacturer((JSONObject)jsonObject.get("manufacturer"));
        }
    }

    public String getID() {
        return ID;
    }

    public String getVIN() {
        return VIN;
    }

    public void setVIN(String VIN) {
        this.VIN = VIN;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public void setManufacturerID(String manufacturerID) {
        ManufacturerID = manufacturerID;
    }

    public Manufacturer getManufacturer() {
        return Manufacturer;
    }

    @Override
    public String toString() {
        String str = "";
        if(ID != null) {
            str += "ID: " + ID;
        }

        str += "\nVIN: " + VIN +
                "\nModel: " + Model +
                "\nColor: " + Color +
                "\nYear: " + Year;

        if(Manufacturer != null) {
            str += "\nManufacturer: " + Manufacturer.getName();
        } else {
            str += "\nManufacturer ID: " + ManufacturerID;
        }

        return str;
    }

    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        if (VIN != null && !VIN.isEmpty()) jsonObject.put("vin", this.VIN);
        if (Model != null && !Model.isEmpty()) jsonObject.put("model", this.Model);
        if (Color != null && !Color.isEmpty()) jsonObject.put("color", this.Color);
        if (Year != null && !Year.isEmpty()) jsonObject.put("year", this.Year);
        if (ManufacturerID != null && !ManufacturerID.isEmpty()) jsonObject.put("manufacturer_id", this.ManufacturerID);

        return jsonObject;
    }
}
