package com.company;

import java.util.List;
import java.util.Scanner;

class Menu {
    private static final Scanner sc = new Scanner(System.in);
    private static APIService apiService;

    public static void login() {
        String email, password;

        System.out.println("\nPlease log in");
        System.out.print("Email: ");
        email = sc.nextLine();
        //System.out.println(email);
        System.out.print("Password: ");
        password = sc.nextLine();
        //System.out.println(password);
        System.out.print("\n");

        try {
            apiService = new APIService(email, password);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Please try again.");
            login();
        }
    }

    public static void logout() {
        try {
            apiService.logout();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void vehicle() {
        Integer selection = 0;
        while(selection != 6) {
            System.out.println("\nVehicle menu");
            System.out.println("1. List");
            System.out.println("2. Select");
            System.out.println("3. Create");
            System.out.println("4. Update");
            System.out.println("5. Delete");
            System.out.println("6. Back");
            selection = sc.nextInt();
            sc.nextLine();

            switch (selection) {
                case 1:
                    try {
                        List<Vehicle> vehicles = apiService.getVehicles();

                        System.out.println("\nAll Vehicles: ");
                        if(vehicles != null) {
                            for (Vehicle v : vehicles) {
                                System.out.println("ID: " + v.getID() + " | "
                                        + v.getYear() + " " + v.getManufacturer().getName() + " " + v.getModel());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 2:
                    try {
                        Vehicle v;
                        System.out.print("Vehicle Id: ");
                        v = apiService.getVehicle(Integer.toString(sc.nextInt()));
                        sc.nextLine();

                        if(v != null) {
                            System.out.println("\nVehicle\n" + v.toString() + "\n");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 3:
                    try {
                        createVehicle();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 4:
                    try {
                        updateVehicle();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 5:
                    try {
                        deleteVehicle();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 6:
                    break;
                default:
                    System.out.println("Please enter a valid option.");
                    break;

            }
        }
    }

    private static void createVehicle() {
        Vehicle v = new Vehicle();
        System.out.println("New Vehicle");
        System.out.print("VIN: ");
        v.setVIN(sc.nextLine());
        System.out.print("Model Name: ");
        v.setModel(sc.nextLine());
        System.out.print("Color: ");
        v.setColor(sc.nextLine());
        System.out.print("Year: ");
        v.setYear(sc.nextLine());
        System.out.print("Manufacturer ID: ");
        v.setManufacturerID(sc.nextLine());

        System.out.println("\nNew Vehicle Summary");
        System.out.println(v.toString());
        System.out.println("\nThis vehicle will be added to the database. Are you sure? (Y/N)");

        switch (sc.nextLine()) {
            case "Y":
            case "y":
                try {
                    apiService.createVehicle(v);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                System.out.println("Record not created.");
                break;
        }
    }

    private static void updateVehicle() {
        Vehicle v;
        System.out.println("Modify Vehicle");
        System.out.print("ID: ");
        v = new Vehicle(String.valueOf(sc.nextInt()));
        sc.nextLine();
        System.out.print("Model Name: ");
        v.setModel(sc.nextLine());
        System.out.print("Color: ");
        v.setColor(sc.nextLine());
        System.out.print("Year: ");
        v.setYear(sc.nextLine());
        System.out.print("Manufacturer ID: ");
        v.setManufacturerID(sc.nextLine());

        System.out.println("\nModify Vehicle Summary");
        System.out.println(v.toString());
        System.out.println("\nThis vehicle will be modified in the database. Are you sure? (Y/N)");

        switch (sc.nextLine()) {
            case "Y":
            case "y":
                try {
                    apiService.updateVehicle(v);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                System.out.println("Record not updated.");
                break;
        }
    }

    private static void deleteVehicle() {
        System.out.println("Delete Vehicle");
        System.out.print("ID: ");
        String vehicleID = String.valueOf(sc.nextInt());
        sc.nextLine();

        System.out.println("\nThis vehicle will be deleted from the database. Are you sure? (Y/N)");

        switch (sc.nextLine()) {
            case "Y":
            case "y":
                try {
                    apiService.deleteVehicle(vehicleID);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                System.out.println("Record not deleted.");
                break;
        }
    }

    public static void manufacturer() {
        Integer selection = 0;
        while(selection != 6) {
            System.out.println("\nManufacturer menu");
            System.out.println("1. List");
            System.out.println("2. Select");
            System.out.println("3. Create");
            System.out.println("4. Update");
            System.out.println("5. Delete");
            System.out.println("6. Back");
            selection = sc.nextInt();
            sc.nextLine();

            switch (selection) {
                case 1:
                    try {
                        List<Manufacturer> manufacturers = apiService.getManufacturers();

                        System.out.println("\nAll Manufacturers: ");
                        for (Manufacturer m : manufacturers) {
                            System.out.println("ID: " + m.getID() + " | " + m.getName());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 2:
                    try {
                        Manufacturer m;
                        System.out.print("Manufacturer Id: ");
                        m = apiService.getManufacturer(Integer.toString(sc.nextInt()));
                        sc.nextLine();

                        if(m != null) {
                            System.out.println("\nManufacturer\n" + m.toString() + "\n");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 3:
                    try {
                        createManufacturer();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 4:
                    try {
                        updateManufacturer();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 5:
                    try {
                        deleteManufacturer();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 6:
                    break;
                default:
                    System.out.println("Please enter a valid option.");
                    break;

            }
        }
    }

    private static void createManufacturer() {
        Manufacturer m = new Manufacturer();
        System.out.println("New Manufacturer");
        System.out.print("Name: ");
        m.setName(sc.nextLine());

        System.out.println("\nNew Manufacturer Summary");
        System.out.println(m.toString());
        System.out.println("\nThis manufacturer will be added to the database. Are you sure? (Y/N)");

        switch (sc.nextLine()) {
            case "Y":
            case "y":
                try {
                    apiService.createManufacturer(m);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                System.out.println("Record not created.");
                break;
        }
    }

    private static void updateManufacturer() {
        Manufacturer m;
        System.out.println("Modify Manufacturer");
        System.out.print("ID: ");
        m = new Manufacturer(String.valueOf(sc.nextInt()));
        sc.nextLine();
        System.out.print("Name: ");
        m.setName(sc.nextLine());

        System.out.println("\nModify Manufacturer Summary");
        System.out.println(m.toString());
        System.out.println("\nThis manufacturer will be modified in the database. Are you sure? (Y/N)");

        switch (sc.nextLine()) {
            case "Y":
            case "y":
                try {
                    apiService.updateManufacturer(m);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                System.out.println("Record not updated.");
                break;
        }
    }

    private static void deleteManufacturer() {
        System.out.println("Delete Manufacturer");
        System.out.print("ID: ");
        String manufacturerID = String.valueOf(sc.nextInt());
        sc.nextLine();

        System.out.println("\nThis manufacturer will be deleted from the database. Are you sure? (Y/N)");

        switch (sc.nextLine()) {
            case "Y":
            case "y":
                try {
                    apiService.deleteManufacturer(manufacturerID);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                System.out.println("Record not deleted.");
                break;
        }
    }
}
