package com.company;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

class APIService {
    private static String Name , Email, APIToken;
    private static final String RootURI = Main.properties.getProperty("endpoint", "http://localhost:8000/api/");

    public APIService(String email, String password) throws IOException {
        login(email, password);
    }

    private void login(String email, String password) throws IOException {
        System.out.println("Logging in...");

        //set endpoint
        String uri = RootURI.concat("login");
        URL url = new URL(uri);

        //create request body
        JSONObject requestBody = new JSONObject();
        requestBody.put("email", email);
        requestBody.put("password", password);

        //Make connection
        HttpURLConnection connection = connect(url, "POST");
        connection.setDoOutput(true);

        sendJSONRequest(connection, requestBody);

        try {
            //read response
            String responseString = getResponse(connection);

            //parse response as JSON Object
            JSONObject obj = (JSONObject)((JSONObject) new JSONParser().parse(responseString)).get("data");

            //Map details to static variables
            APIToken = obj.get("api_token").toString();
            Name = obj.get("name").toString();
            Email = obj.get("email").toString();

            System.out.println("\nWelcome " + Name);

        } catch(ParseException pe) {
            System.out.println("Error reading response.");
        }
    }

    public void logout() throws IOException {
        System.out.println("Logging out...");

        //set endpoint
        String uri = RootURI.concat("logout");
        URL url = new URL(uri);

        //Make connection
        HttpURLConnection connection = connect(url, "POST");

        //read response
        getResponse(connection);

        //Map details to static variables
        APIToken = null;
        Name = null;
        Email = null;

    }

    public List<Vehicle> getVehicles() throws IOException, ParseException {
        System.out.println("Retrieving vehicles...");

        List<Vehicle> vehicles = new ArrayList<>();

        //set endpoint
        String uri = RootURI.concat("vehicles");
        URL url = new URL(uri);

        //Make connection
        HttpURLConnection connection = connect(url, "GET");

        String responseString = getResponse(connection);
        if(connection.getResponseCode() == 200) {

            //parse response as JSON Object
            JSONArray objArray = (JSONArray) new JSONParser().parse(responseString);

            for (JSONObject jsonObject : (List<JSONObject>) objArray) {
                vehicles.add(new Vehicle(jsonObject));
            }

            return vehicles;
        }

        return null;
    }

    public Vehicle getVehicle(String Id) throws IOException, ParseException {
        System.out.println("Finding vehicle...");

        //set endpoint
        String uri = RootURI.concat("vehicles/" + Id);
        URL url = new URL(uri);

        //Make connection
        HttpURLConnection connection = connect(url, "GET");

        String responseString = getResponse(connection);

        if(connection.getResponseCode() == 200) {
            //parse response as JSON Object and return vehicle
            return new Vehicle((JSONObject) new JSONParser().parse(responseString));
        }
        return null;
    }

    public void createVehicle(Vehicle vehicle) throws IOException {
        System.out.println("Creating new vehicle...");

        //set endpoint
        String uri = RootURI.concat("vehicles");
        URL url = new URL(uri);

        //Make connection
        HttpURLConnection connection = connect(url, "POST");
        connection.setDoOutput(true);

        //send Request Body
        sendJSONRequest(connection, vehicle.toJSON());

        //return response
        getResponse(connection);
    }

    public void updateVehicle(Vehicle vehicle) throws IOException {
        System.out.println("Updating vehicle...");

        //set endpoint
        String uri = RootURI.concat("vehicles/" + vehicle.getID());
        URL url = new URL(uri);

        HttpURLConnection connection = connect(url, "PUT");
        connection.setDoOutput(true);
        sendJSONRequest(connection, vehicle.toJSON());

        //get response as string
        getResponse(connection);

    }

    public void deleteVehicle(String vehicleID) throws IOException {
        System.out.println("Deleting vehicle...");

        //set endpoint
        String uri = RootURI.concat("vehicles/" + vehicleID);
        URL url = new URL(uri);

        HttpURLConnection connection = connect(url, "DELETE");

        //get response as string
        getResponse(connection);
    }

    public List<Manufacturer> getManufacturers() throws IOException, ParseException {
        System.out.println("Retrieving manufacturers...");

        List<Manufacturer> manufacturers = new ArrayList<>();

        //set endpoint
        String uri = RootURI.concat("manufacturers");
        URL url = new URL(uri);

        //Make connection
        HttpURLConnection connection = connect(url, "GET");

        String responseString = getResponse(connection);

        if(connection.getResponseCode() == 200) {
            //parse response as JSON Object
            JSONArray objArray = (JSONArray) new JSONParser().parse(responseString);

            for (JSONObject jsonObject : (List<JSONObject>)objArray) {
                manufacturers.add(new Manufacturer(jsonObject));
            }

            return manufacturers;
        }
        return null;
    }

    public Manufacturer getManufacturer(String Id) throws IOException, ParseException {
        System.out.println("Finding manufacturer...");

        //set endpoint
        String uri = RootURI.concat("manufacturers/" + Id);
        URL url = new URL(uri);

        //Make connection
        HttpURLConnection connection = connect(url, "GET");

        String responseString = getResponse(connection);

        if(connection.getResponseCode() == 200) {
            //parse response as JSON Object and return manufacturer
            return new Manufacturer((JSONObject) new JSONParser().parse(responseString));
        }
        return null;
    }

    public void createManufacturer(Manufacturer manufacturer) throws IOException {
        System.out.println("Creating new manufacturer...");

        //set endpoint
        String uri = RootURI.concat("manufacturers");
        URL url = new URL(uri);

        //Make connection
        HttpURLConnection connection = connect(url, "POST");
        connection.setDoOutput(true);
        //send Request Body
        sendJSONRequest(connection, manufacturer.toJSON());

        //get response as string
        getResponse(connection);
    }

    public void updateManufacturer(Manufacturer manufacturer) throws IOException {
        System.out.println("Updating manufacturer...");

        //set endpoint
        String uri = RootURI.concat("manufacturers/" + manufacturer.getID());
        URL url = new URL(uri);

        HttpURLConnection connection = connect(url, "PUT");
        connection.setDoOutput(true);
        //Send Request body
        sendJSONRequest(connection, manufacturer.toJSON());

        //get response as string
        getResponse(connection);
    }

    public void deleteManufacturer(String manufacturerID) throws IOException {
        System.out.println("Deleting manufacturer...");

        //set endpoint
        String uri = RootURI.concat("manufacturers/" + manufacturerID);
        URL url = new URL(uri);

        HttpURLConnection connection = connect(url, "DELETE");

        getResponse(connection);
    }

    private void sendJSONRequest(HttpURLConnection connection, JSONObject jsonObject) throws IOException  {
        OutputStream os = connection.getOutputStream();
        os.write(jsonObject.toString().getBytes());
        os.flush();
        os.close();
    }

    private HttpURLConnection connect(URL url, String requestMethod) throws IOException {
        //Make connection
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod(requestMethod);
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("Content-type", "application/json");
        connection.setRequestProperty("Authorization", "Bearer " + APIToken);

        return connection;
    }

    private String getResponse(HttpURLConnection connection) {
        String response;

        try {
            response = readInputStream(connection.getInputStream());
            System.out.println("Success.");

            return response;
        } catch (Exception e) {
            response = readInputStream(connection.getErrorStream());
            System.out.println("\nError: " + response + "\n");
        }

        try {
            if (connection.getResponseCode() == 401) {
                resetConnection();
            }
        } catch (IOException ioe) {
            System.out.println("There was a problem connecting to the server.");
        }

        return null;
    }

    private String readInputStream(InputStream is) {
        //read response
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        String inputLine;
        StringBuffer response = new StringBuffer();

        try {
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return String.valueOf(response);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private static void resetConnection() {
        Name = null;
        Email = null;
        APIToken = null;

        Menu.login();
    }
}
